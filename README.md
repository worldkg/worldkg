## Motivation

Knowledge graphs provide rich semantic representations of real-world entities and their relations. Whereas popular general-purpose knowledge graphs such as Wikidata and DBpedia contain selected geographic entities, their coverage of geographic information is limited. An essential source of openly available geographic information is OpenStreetMap (OSM). In contrast to knowledge graphs, OSM lacks a clear semantic representation of the rich geographic information it contains. Generation of semantic representations of OSM entities and their interlinking with knowledge graphs are inherently challenging due to the large, heterogeneous, ambiguous, and flat OSM schema and the annotation sparsity.

The objectives of the WoldKG project include:
1. Development of methods for world-scale alignment of OSM datasets and knowledge graphs at the schema and instance level.
2. Preparation and release of data resulting from these methods in the form of the WorldKG knowledge graph that provides a comprehensive semantic representation of geographical information and its context.

The results of the WorldKG project can substantially benefit applications in mobility, transportation, tourism, and logistics domains, as well as provide a basis for informational maps and services through the high-quality semantic representation of geographical and contextual information.
## Project Results

As a result of the WorldKG project, we developed methods for knowledge graph completion, establishing links between OSM and knowledge graphs at the entity and schema levels. In particular, we created the OSM2KG approach aimed at interlinking geographic entities in OSM and knowledge graphs. Furthermore, we presented a novel Neural Class Alignment (NCA) algorithm to link the OSM tags providing entity-type information to the corresponding knowledge graph classes. As a result of these methods, we semantically enriched geospatial entities in OSM and made these entities available in WorldKG - a novel geographic knowledge graph. Furthermore, we developed methods and datasets to enhance data quality in OSM and to make OSM data available to machine learning applications. Moreover, we developed a number of predictive models and applications building upon these datasets.

<figure>
            <img class="img_text"
                src="https://gitlab.vgiscience.de/worldkg/worldkg/-/raw/master/Images/OverviewWorldKG.png">
            <figcaption>Overview of the pipeline for creating WorldKG from OSM, consisting of three main steps: (i) Geographic entity linking with OSM2KG, (ii) geographic class alignment with NCA, and (iii) WorldKG geographic knowledge graph creation.
            </figcaption>
</figure>



## Selected Contributions

* WorldKG Knowledge Graph
    * Description: WorldKG is a geographic knowledge graph  providing a comprehensive semantic representation of geographic entities in OpenStreetMap. The WorldKG knowledge graph is built according to the WorldKG ontology, providing its semantic backbone.
    * [Paper](https://dl.acm.org/doi/abs/10.1145/3459637.3482023)  [PDF](https://arxiv.org/pdf/2109.10036.pdf)
    * [Website](https://www.worldkg.org/kg)
    * [Code](https://github.com/alishiba14/WorldKG-Knowledge-Graph)
* Neural Class Alignment (NCA)
    * Description: Interlinking OSM entities with knowledge graphs is inherently difficult due to the large, heterogeneous, ambiguous, and flat OSM schema and the annotation sparsity. NCA holistically aligns OSM tags with the corresponding knowledge graph classes by jointly considering the schema and instance layers. It trains a novel neural architecture that capitalizes upon a shared latent space for tag-to-class alignment created using linked entities in OSM and knowledge graphs.
    * [Paper](https://link.springer.com/chapter/10.1007/978-3-030-88361-4_4) [PDF](https://arxiv.org/pdf/2107.13257.pdf)
    * [Code](https://github.com/alishiba14/NCA-OSM-to-KGs)
* Linking OSM to Knowledge Graphs (OSM2KG) 
    * Description: OSM2KG - a link discovery approach predicts identity links between OSM nodes and geographic entities in a knowledge graph. The core of the OSM2KG approach is a latent representation of OSM nodes that captures semantic node similarity in an embedding. OSM2KG adopts this latent representation to train a supervised model for link prediction and utilizes existing links between OSM and knowledge graphs for training.
    * [Paper](https://www.sciencedirect.com/science/article/abs/pii/S0167739X20330272) [PDF](https://arxiv.org/pdf/2011.05841.pdf)
    * [Code](https://github.com/NicolasTe/osm2kg)
* GeoVectors corpus of OSM entity embeddings 
    * Description: GeoVectors – a comprehensive world-scale linked open corpus of OSM entity embeddings covers the entire OSM dataset and provides latent representations of over 980 million geographic entities in 180 countries. The GeoVectors corpus captures the semantic and geographic dimensions of OSM entities and makes these entities directly accessible to machine learning algorithms and semantic applications.
    * [Paper](https://dl.acm.org/doi/abs/10.1145/3459637.3482004) [PDF](https://arxiv.org/pdf/2108.13092.pdf)
    * [Website](https://geovectors.l3s.uni-hannover.de/)
    * [Code](https://github.com/NicolasTe/GeoVectors)
* Attention-Based Vandalism Detection in OpenStreetMap (OVID)
    * Description: Ovid is an attention-based method for vandalism detection in OSM. Ovid relies on a neural architecture that adopts a multi-head attention mechanism to effectively summarize information indicating vandalism from OSM changesets. To facilitate automated vandalism detection, OVID introduces a set of original features that capture changeset, user, and edit information. Furthermore, a dataset of real-world vandalism incidents from the OSM edit history is provided as open data.
    * [Paper](https://dl.acm.org/doi/10.1145/3485447.3512224) [PDF](https://arxiv.org/pdf/2201.10406.pdf)
    * [Code](https://github.com/NicolasTe/Ovid)

[^1]: OpenStreetMap, OSM, and the OpenStreetMap magnifying glass logo are trademarks of the OpenStreetMap Foundation and are used with their permission. We are not endorsed by or affiliated with the OpenStreetMap Foundation.